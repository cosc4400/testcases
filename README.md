Tests
=====

This is a test case repository for projects in COSC4400.  To be given write access, let us know your bitbucket or github username.


Planned File Structure
----------------------
minijava programs split into different folders by type:
* valid minijava programs 
* invalid minijava programs -- to test error cases
* ? -- feel free to add folders/categories


Planned Branch Structure
------------------------
probably just master


Brylowdiff
----------
It's a tool takes some input and compares the output of your implementation and the reference implementation.  It may need to be configured to find your working directory. Tested on morbius, but you will need libgcj and python to run it elsewhere. You get more colors if you have colordiff.  
`./brylowdiff.py <input file or directory>`

If run on a directory, it will recurse and output a quick pass/fail on each file it finds.  
`./brylowdiff.py .`  
should run every test here.

Running on a file prints a full side by side diff.
