
public class BinaryOpTester {

	int b1 = 0010100101;
	int b2 = 1111111111;
	int b3 = 0100101001;
	int b4;
	int b5;
	int b6;

	void testmethodsimple(){	
		b4 = b1 ^ b2;
		b5 = b2 & b3;
		b6 = b1 | b3;
		b5 = ~ b1;
	}
	
	void testnested(){
		abdc.ke|kjio;
		void&1&letters;
		newlinete,st&
		&;
		doubl.e|stuckin,between|test
		Pri,orityTest &&&;
		Pr,iorit.yEquals ===;
		Prio,rityNot =!=;
		
		Brackets all over the place
		( {)} [(] )  &[&]
				
		Simple ma,ths
		variable ++;
		vari,able += bananas
				Student - brains
				42 * 53
				alpha /
				beta
				
				Greaterthan.les,sthanpriority
				<==>Endpriority
				Bro,kenp.riorit,ycheck
				<=
				=> endbrokenpriority
		Interrupted Identi85!32fier check
		int errupted .reserved word check ch&ar.
		A s.ma,ttering of punct.uation throughout.
	}
	
	Block comment following
	
	/*
	 * 
	 * 
	 * 
	 * comment comment comment
	 
	 
	 
	 No asterisks comment
	 
	 */Ended comment.
	 */
}
