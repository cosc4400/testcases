class Main{
public static void main(String[] args){

//Expression.ID
Expr = Ident.Id.Id;
Expr = Ident.Id.Id(Ident);
Expr = Ident.Id.Id(Ident, Ident);
Expr = Ident.Id||Ident;
Expr = Ident.Id&&Ident;
Expr = Ident.Id==Ident;
Expr = Ident.Id>Ident;
Expr = Ident.Id<Ident;
Expr = Ident.Id+Ident;
Expr = Ident.Id-Ident;
Expr = Ident.Id*Ident;
Expr = Ident.Id/Ident;
Expr = Ident.Id[Ident];
Expr = new Type [Ident.Id];
Expr = new Type [Ident.Id][];
Expr = new Type [Ident.Id][][][];
Expr = Xinu.Id(Ident.Id);
Expr = Xinu.Id(Ident.Id, Ident.Id);
Expr = -Ident.Id;
Expr = !Ident.Id;
Expr = (Ident.Id);


//

//||
Expr = Ident || Ident.Id;
Expr = Ident || Ident.Id();
Expr = Ident || Ident.Id(Expr);
Expr = Ident || Ident.Id(Expr, Expr);
Expr = Ident || Ident || Ident;
Expr = Ident || Ident && Ident;
Expr = Ident || Ident == Ident;
Expr = Ident || Ident > Ident;
Expr = Ident || Ident < Ident;
Expr = Ident || Ident + Ident;
Expr = Ident || Ident - Ident;
Expr = Ident || Ident * Ident;
Expr = Ident || Ident / Ident;
Expr = Ident || Ident [Ident];
Expr = Ident || new Type();
Expr = Ident || new Type[Expr];
Expr = Ident || new Type[Expr][];
Expr = Ident || new Type[Expr][][][];
Expr = Ident || Xinu.Id(Expr);
Expr = Ident || Xinu.Id(Expr, Expr);
Expr = Ident || -Ident;
Expr = Ident || !Ident;
Expr = Ident || (Ident);

//&&
Expr = Ident && Ident.Id;
Expr = Ident && Ident.Id();
Expr = Ident && Ident.Id(Expr);
Expr = Ident && Ident.Id(Expr, Expr);
Expr = Ident && Ident || Ident;
Expr = Ident && Ident && Ident;
Expr = Ident && Ident == Ident;
Expr = Ident && Ident > Ident;
Expr = Ident && Ident < Ident;
Expr = Ident && Ident + Ident;
Expr = Ident && Ident - Ident;
Expr = Ident && Ident * Ident;
Expr = Ident && Ident / Ident;
Expr = Ident && Ident [Ident];
Expr = Ident && new Type();
Expr = Ident && new Type[Expr];
Expr = Ident && new Type[Expr][];
Expr = Ident && new Type[Expr][][][];
Expr = Ident && Xinu.Id(Expr);
Expr = Ident && Xinu.Id(Expr, Expr);
Expr = Ident && -Ident;
Expr = Ident && !Ident;
Expr = Ident && (Ident);

//==
Expr = Ident == Ident.Id;
Expr = Ident == Ident.Id();
Expr = Ident == Ident.Id(Expr);
Expr = Ident == Ident.Id(Expr, Expr);
Expr = Ident == Ident || Ident;
Expr = Ident == Ident && Ident;
Expr = Ident == Ident == Ident;
Expr = Ident == Ident > Ident;
Expr = Ident == Ident < Ident;
Expr = Ident == Ident + Ident;
Expr = Ident == Ident - Ident;
Expr = Ident == Ident * Ident;
Expr = Ident == Ident / Ident;
Expr = Ident == Ident [Ident];
Expr = Ident == new Type();
Expr = Ident == new Type[Expr];
Expr = Ident == new Type[Expr][];
Expr = Ident == new Type[Expr][][][];
Expr = Ident == Xinu.Id(Expr);
Expr = Ident == Xinu.Id(Expr, Expr);
Expr = Ident == -Ident;
Expr = Ident == !Ident;
Expr = Ident == (Ident);

//>
Expr = Ident > Ident.Id;
Expr = Ident > Ident.Id();
Expr = Ident > Ident.Id(Expr);
Expr = Ident > Ident.Id(Expr, Expr);
Expr = Ident > Ident || Ident;
Expr = Ident > Ident && Ident;
Expr = Ident > Ident == Ident;
Expr = Ident > Ident > Ident;
Expr = Ident > Ident < Ident;
Expr = Ident > Ident + Ident;
Expr = Ident > Ident - Ident;
Expr = Ident > Ident * Ident;
Expr = Ident > Ident / Ident;
Expr = Ident > Ident [Ident];
Expr = Ident > new Type();
Expr = Ident > new Type[Expr];
Expr = Ident > new Type[Expr][];
Expr = Ident > new Type[Expr][][][];
Expr = Ident > Xinu.Id(Expr);
Expr = Ident > Xinu.Id(Expr, Expr);
Expr = Ident > -Ident;
Expr = Ident > !Ident;
Expr = Ident > (Ident);

//<
Expr = Ident < Ident.Id;
Expr = Ident < Ident.Id();
Expr = Ident < Ident.Id(Expr);
Expr = Ident < Ident.Id(Expr, Expr);
Expr = Ident < Ident || Ident;
Expr = Ident < Ident && Ident;
Expr = Ident < Ident == Ident;
Expr = Ident < Ident > Ident;
Expr = Ident < Ident < Ident;
Expr = Ident < Ident + Ident;
Expr = Ident < Ident - Ident;
Expr = Ident < Ident * Ident;
Expr = Ident < Ident / Ident;
Expr = Ident < Ident [Ident];
Expr = Ident < new Type();
Expr = Ident < new Type[Expr];
Expr = Ident < new Type[Expr][];
Expr = Ident < new Type[Expr][][][];
Expr = Ident < Xinu.Id(Expr, Expr);
Expr = Ident < -Ident;
Expr = Ident < !Ident;
Expr = Ident < (Ident);

//+
Expr = Ident + Ident.Id;
Expr = Ident + Ident.Id();
Expr = Ident + Ident.Id(Expr);
Expr = Ident + Ident.Id(Expr, Expr);
Expr = Ident + Ident || Ident;
Expr = Ident + Ident && Ident;
Expr = Ident + Ident == Ident;
Expr = Ident + Ident > Ident;
Expr = Ident + Ident < Ident;
Expr = Ident + Ident + Ident;
Expr = Ident + Ident - Ident;
Expr = Ident + Ident * Ident;
Expr = Ident + Ident / Ident;
Expr = Ident + Ident [Ident];
Expr = Ident + new Type();
Expr = Ident + new Type[Expr];
Expr = Ident + new Type[Expr][];
Expr = Ident + new Type[Expr][][][];
Expr = Ident + Xinu.Id(Expr);
Expr = Ident + Xinu.Id(Expr, Expr);
Expr = Ident + -Ident;
Expr = Ident + !Ident;
Expr = Ident + (Ident);

//-
Expr = Ident - Ident.Id;
Expr = Ident - Ident.Id();
Expr = Ident - Ident.Id(Expr);
Expr = Ident - Ident.Id(Expr, Expr);
Expr = Ident - Ident || Ident;
Expr = Ident - Ident && Ident;
Expr = Ident - Ident == Ident;
Expr = Ident - Ident > Ident;
Expr = Ident - Ident < Ident;
Expr = Ident - Ident + Ident;
Expr = Ident - Ident - Ident;
Expr = Ident - Ident * Ident;
Expr = Ident - Ident / Ident;
Expr = Ident - Ident [Ident];
Expr = Ident - new Type();
Expr = Ident - new Type[Expr];
Expr = Ident - new Type[Expr][];
Expr = Ident - new Type[Expr][][][];
Expr = Ident - Xinu.Id(Expr);
Expr = Ident - Xinu.Id(Expr, Expr);
Expr = Ident - -Ident;
Expr = Ident - !Ident;
Expr = Ident - (Ident);

//*
Expr = Ident * Ident.Id;
Expr = Ident * Ident.Id();
Expr = Ident * Ident.Id(Expr);
Expr = Ident * Ident.Id(Expr, Expr);
Expr = Ident * Ident || Ident;
Expr = Ident * Ident && Ident;
Expr = Ident * Ident == Ident;
Expr = Ident * Ident > Ident;
Expr = Ident * Ident < Ident;
Expr = Ident * Ident + Ident;
Expr = Ident * Ident - Ident;
Expr = Ident * Ident * Ident;
Expr = Ident * Ident / Ident;
Expr = Ident * Ident [Ident];
Expr = Ident * new Type();
Expr = Ident * new Type[Expr];
Expr = Ident * new Type[Expr][];
Expr = Ident * new Type[Expr][][][];
Expr = Ident * Xinu.Id(Expr);
Expr = Ident * Xinu.Id(Expr, Expr);
Expr = Ident * -Ident;
Expr = Ident * !Ident;
Expr = Ident * (Ident);

//	/
Expr = Ident / Ident.Id;
Expr = Ident / Ident.Id();
Expr = Ident / Ident.Id(Expr);
Expr = Ident / Ident.Id(Expr, Expr);
Expr = Ident / Ident || Ident;
Expr = Ident / Ident && Ident;
Expr = Ident / Ident == Ident;
Expr = Ident / Ident > Ident;
Expr = Ident / Ident < Ident;
Expr = Ident / Ident + Ident;
Expr = Ident / Ident - Ident;
Expr = Ident / Ident * Ident;
Expr = Ident / Ident / Ident;
Expr = Ident / Ident [Ident];
Expr = Ident / new Type();
Expr = Ident / new Type[Expr];
Expr = Ident / new Type[Expr][];
Expr = Ident / new Type[Expr][][][];
Expr = Ident / Xinu.Id(Expr);
Expr = Ident / Xinu.Id(Expr, Expr);
Expr = Ident / -Ident;
Expr = Ident / !Ident;
Expr = Ident / (Ident);

//	[]
Expr = Ident [Ident.Id];
Expr = Ident [Ident.Id()];
Expr = Ident [Ident.Id(Expr)];
Expr = Ident [Ident.Id(Expr, Expr)];
Expr = Ident [Ident || Ident];
Expr = Ident [Ident && Ident];
Expr = Ident [Ident == Ident];
Expr = Ident [Ident > Ident];
Expr = Ident [Ident < Ident];
Expr = Ident [Ident + Ident];
Expr = Ident [Ident - Ident];
Expr = Ident [Ident * Ident];
Expr = Ident [Ident / Ident];
Expr = Ident [Ident [Ident]];
Expr = Ident [new Type()];
Expr = Ident [new Type[Expr]];
Expr = Ident [new Type[Expr][][][]];
Expr = Ident [Xinu.Id(Expr)];
Expr = Ident [Xinu.Id(Expr, Expr)];
Expr = Ident [-Ident];
Expr = Ident [!Ident];
Expr = Ident [(Ident)];
}
}
