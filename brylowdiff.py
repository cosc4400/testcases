#! /usr/bin/python
import sys
import os.path
from os import listdir
import fileinput
import subprocess
from subprocess import call

TEST_DIR = '../'
TEST_CMD = 'Parse.Main'
REF_CMD = ".reference/parser"

def testFile(filename):
    call([REF_CMD, filename], stdout=open('.ref.data','w'), stderr=open(os.devnull, 'w'))
    call(["java", '-cp', TEST_DIR, TEST_CMD, filename], stdout=open('.test.data', 'w'), stderr=open(os.devnull, 'w'))

    booldiff,err = subprocess.Popen(["diff", "-q", ".test.data", ".ref.data"], stdout=subprocess.PIPE).communicate()
    file1 = set(line.strip() for line in open('.test.data').read().split())
    file2 = set(line.strip() for line in open('.ref.data').read().split())
    exstr = ""
    for word in file1 & file2: 
        if 'exception' in word.lower(): 
            exstr = word[:-1]
            break

    color = "";
    if (booldiff == ""): #pass
        color = '\033[92m' #green
    elif (exstr != ""): #pass w/ exception
        color = '\033[93m' #yellow
    else: #fail
        color = '\033[91m' #red

    print(color + filename + '\033[0m' + ((' -> ' + exstr + '\033[0m') if (exstr != '') else ''))

    os.remove(".test.data")
    os.remove(".ref.data")
    if (os.path.isfile('.test.data') or os.path.isfile('.ref.data')): print("failed to delete temp files")
    return

def testDir(dirname):
    dirs = [] 
    print(dirname)
    for f in os.listdir(dirname):
       if (f[0] == '.' or 'readme' in f.lower()): continue #don't recurse on hidden dirs or files
       f = dirname + f
       if (os.path.isfile(f)):
           testFile(f)
       else: 
           dirs.append(os.path.join(f, '')) #add trailing /
    for d in dirs:
        testDir(d)
    return


if ('tests' in os.listdir(os.getcwd())):
    os.chdir('tests')

if (len(sys.argv) < 2):
   print("Usage: " + sys.argv[0] + " [file]")
   sys.exit()

file_index = 1



filename = sys.argv[file_index]

if (not os.path.isfile(REF_CMD)):
   print("brylow executable " + REF_CMD + " cannot be found");
   sys.exit()

if (not os.path.isfile(TEST_DIR + TEST_CMD.replace('.','/') + ".class")):
   print("implementation under test cannot be found");
   sys.exit()

if os.path.isfile(".stdin.data"): os.remove(".stdin.data")
if os.path.isfile(".ref.data"): os.remove(".ref.data")
if os.path.isfile(".test.data"): os.remove(".test.data")
if os.path.isfile("scanner.diff"): os.remove("scanner.diff")

if (filename == '-'):
   save = open('.stdin.data', 'w')
   for line in fileinput.input():
      save.write(line)
   save.close()
   filename = '.stdin.data'
else:
   if (not os.path.isfile(filename) and not os.path.isdir(filename)):
      print("input file not found")
      sys.exit()

if (os.path.isdir(filename)):
    filename = os.path.join(filename, '')#add / if not present
    testDir(filename)
else:
    call([REF_CMD, filename], stdout=open('.ref.data','w'))
    call(["java", "-cp", TEST_DIR, TEST_CMD, filename], stdout=open('.test.data','w'))
    call(["diff", "-y", ".test.data", ".ref.data"], stdout=open('.scanner.diff', 'w'))
    print("test -> reference")

    if (os.path.isfile("/usr/bin/colordiff")):
        cat = subprocess.Popen(["cat", ".scanner.diff"], stdout=subprocess.PIPE)
        color = subprocess.Popen(["colordiff"], stdin=cat.stdout)
        output,err = color.communicate()
    else:
        subprocess.call(["cat", ".scanner.diff"])

    print('---')
    testFile(filename)
